package com.midterm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.midterm.entity.Member;
import com.midterm.repository.MemberRepository;
import com.midterm.service.MemberService;

@Service
public class MemberServiceImpl implements MemberService {

	@Autowired
	private MemberRepository memberRepository;

	@Override
	public List<Member> getList() {
		return memberRepository.findAll();
	}

	@Override
	public Member getById(Long id) {
		return memberRepository.findById(id).get();
	}

	@Override
	public Member create(Member student) {
		return memberRepository.save(student);
	}

	@Override
	public List<Member> getByFemale(boolean female) {
		return memberRepository.findByFemale(female);
	}
}
