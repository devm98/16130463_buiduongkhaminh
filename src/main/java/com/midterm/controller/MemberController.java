package com.midterm.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.midterm.entity.Member;
import com.midterm.service.MemberService;

@Controller
public class MemberController {
	@Autowired
	private MemberService memberService;

	@GetMapping("")
	public String index(Model model) {
		model.addAttribute("members", memberService.getList());
		return "memberList";
	}

	@GetMapping("/memberDetail")
	public String memberDetail(Model model, @RequestParam("id") Long id) {
		model.addAttribute("member", memberService.getById(id));
		return "memberDetail";
	}
	
	@GetMapping("/filter")
	public String filter(Model model,  @RequestParam("female") boolean female) {
		model.addAttribute("members", memberService.getByFemale(female));
		
		return "resultSearch";
	}
	

	@GetMapping("/showFormNew")
	public String showFormNew(Model model) {
		Member member = new Member();
		model.addAttribute("member", member);
		return "registerMember";
	}

	@RequestMapping(value = "/newMember", method = RequestMethod.POST)
	public String newMember(@ModelAttribute @Valid Member member, Errors errors, Model model) {
		try {
			if (null != errors && errors.getErrorCount() > 0) {
				return "registerMember";
			} else {
				memberService.create(member);
				model.addAttribute("member", member);
				return "success";
			}
		} catch (Exception e) {
			model.addAttribute("errorMg", "Email này đã có người đăng ký. Thử lại.");
			return "registerMember";
		}

	}

}
