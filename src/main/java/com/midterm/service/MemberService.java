package com.midterm.service;

import java.util.List;

import com.midterm.entity.Member;

public interface MemberService {
	List<Member> getList();

	List<Member> getByFemale(boolean female);
	
	Member getById(Long id);

	Member create(Member student);
}
