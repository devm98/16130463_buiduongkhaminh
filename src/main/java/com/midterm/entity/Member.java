package com.midterm.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "members")
public class Member {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(unique = true)
	@NotEmpty(message = "Bạn không thể để trống dữ liệu này")
	@Email(message = "Email không hợp lệ. Thử lại.")
	private String email;
	
	@NotEmpty(message = "Bạn không thể để trống dữ liệu này")
	@Size(min = 8, message = "Mật khẩu có ít nhất 8 ký tự.Thử lại.")
	private String password;
	
	@NotEmpty(message = "Bạn không thể để trống dữ liệu này")
	private String name;

	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@NotNull(message = "Bạn không thể để trống dữ liệu này")
	private Date birthDate;

	private boolean female;
	private String degree;
	private String address;
	private String phone;

	@Transient
	@NotEmpty(message = "Bạn không thể để trống dữ liệu này")
	private String repassword;

	public Member() {
	}

	public Member(String email, String password, String name, Date birthDate, boolean female, String degree,
			String address, String phone) {
		super();
		this.email = email;
		this.password = password;
		this.name = name;
		this.birthDate = birthDate;
		this.female = female;
		this.degree = degree;
		this.address = address;
		this.phone = phone;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public boolean isFemale() {
		return female;
	}

	public void setFemale(boolean female) {
		this.female = female;
	}

	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getRepassword() {
		return repassword;
	}

	public void setRepassword(String repassword) {
		this.repassword = repassword;
	}

}
